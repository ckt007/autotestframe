import os

import pytest
from src.utils import config_util

if __name__ == '__main__':
    run_args=config_util.get_run_cases()
    if len(run_args)==0:
        raise Exception('not find run case')
    run_args.extend(['-o','log_cli=true','-v','--alluredir','./report/xml'])
    pytest.main(run_args)


#华为机：5DEDU18519015261