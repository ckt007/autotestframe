import allure #pip install scikit-image -i https://pypi.tuna.tsinghua.edu.cn/simple
import pytest #scipy
from xray.api import *
from xray.device import *
from src.base.test_fliggy_base import TestFliggyBase

class TestMain(TestFliggyBase):
    @allure.feature('酒店')
    @allure.story('酒店服务入口')
    @allure.feature('normal')
    def test_hotel_service_entrance(self):
        TestFliggyBase.open_order_detail('','酒店')
        sleep(1.0)
        ENV.refresh()#截图
        sleep(1.0)

        assert exists('联系飞猪')
        touch('联系飞猪',exactly_match=False)

if __name__ == '__main__':
    ars=['test_hotel_service_entrance.py','-o','log_cli=true','-v','--alluredir','./report/xml']
    pytest.main(args)