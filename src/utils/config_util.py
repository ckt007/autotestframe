import json
import os
from src.utils import path_util


def get_config_path():
    root_path=path_util.get_project_root_path()
    return os.path.join(root_path,'config.json')

def get_config():
    with open(get_config_path(),'r') as f:
        config=json.load(f)
        return config

def get_config_value(key):
    config=get_config()
    return config.get(key)

def get_test_cases():
    return get_config_value("testCases")

def get_run_cases():
    run_cases=get_config_value("runCases")
    #print(run_cases)
    test_cases=get_test_cases()
    #print(test_cases)
    return [case["casePath"] for case in test_cases if case["caseKey"] in run_cases]