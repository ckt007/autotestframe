import os

def get_project_root_path():
    """
    获取项目路径
    :return:
    """
    return os.path.dirname(os.path.dirname(os.path.dirname(__file__)))