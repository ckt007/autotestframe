import os
from xray.device import *
from src.utils.config_util import get_config_value


class TestBase(object):
    device_url=get_config_value('deviceUrl')

    @staticmethod
    def connect():
        assert connect_device(TestBase.device_url)

    @staticmethod
    def get_current_case_name():
        return os.environ.get('PYTEST_CURRENT_TEST').split(':')[-1].split(' ')[0]

    @staticmethod
    def create_case_dir():
        custom_child_dir=TestBase.get_current_case_name()
        ENV.set_custom_intermediates_child_dir(custom_child_dir)