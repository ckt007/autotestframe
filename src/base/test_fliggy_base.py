from fliggyAuto.tools import auto_tools as fliggy_tools
from airtest.core.api import *
from xray.api import *
from src.base.test_base import *
import os

class TestFliggyBase(object):
    device_url=get_config_value('deviceUrl')
    app_package = get_config_value('appPackage')

    @staticmethod
    def setup_class():
        TestBase.connect()
        ENV.refresh()  # 截图

    @staticmethod
    def setup():
        TestBase.create_case_dir()
        fliggy_tools.start_fliggy()
        sleep(1.0)

    @staticmethod
    def teardown_class():
        sleep(1.0)

    @staticmethod
    def teardown():
        fliggy_tools.stop_fliggy()
        sleep(1.0)

    @staticmethod
    def start_fliggy_app_first():
        TestBase.create_case_dir()
        fliggy_tools.start_fliggy()
        sleep(1.0)